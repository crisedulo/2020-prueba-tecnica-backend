const express = require("express");

const app = express.Router();

const swaggerUi = require("swagger-ui-express"),
  swaggerDocument = require("../swagger/swagger.json");

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.get("/", function (req, res, next) {
  res.redirect("/api-docs");
});

/**
 * @swagger
 * /tests/borrarme:
 *   get:
 *     summary: "Obtener telefono a través del identificador"
 *     description: Devuelve los datos del telefono si se encuentra habilitada en base de datos.
 *     tags: [TESTS/BORRARME]
 *     security:
 *     parameters:
 *        
 *     responses:
 *       200:
 *         description: Ok
 *       401:
 *         description: No autorizado.
 *       400:
 *         description: Error en la validación o Error en los Headers.
 */

module.exports = app;
