require('dotenv').config()

module.exports = {
  // ------------- CONSTANTES PROPIAS --------------
  allowMethods: "GET,PUT,POST,DELETE",
  allowHeaders:
    "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-filename, Content-disposition, system, action, cui, rol, env",
  allowExpose: "x-filename, Content-disposition",
  allowOrigin: process.env.ALLOW_ORIGIN,
  // ------------- CONSTANTES INYECTABLES --------------
  //Global variables
  host: process.env.HOST,
  port: parseInt(process.env.PORT),
  messageTerminal: process.env.MESSAGE_TERMINAL,
  nodeEnvironment: process.env.NODE_ENV,

  //SQLServer database
  sqlServerName: process.env.SQLSERVER_NAME,
  sqlServerUsername: process.env.SQLSERVER_USERNAME,
  sqlServerPassword: process.env.SQLSERVER_PASSWORD,
  sqlServerPort: parseInt(process.env.SQLSERVER_PORT),
  sqlServerHost: process.env.SQLSERVER_HOST,
  sqlServerInstance: process.env.SQLSERVER_INSTANCE,
  sqlServerDialect: process.env.SQLSERVER_DIALECT,
  sqlServerLogging: !!parseInt(process.env.SQLSERVER_LOGGING),
  sqlServerOperatorsAliases: !!parseInt(
    process.env.SQLSERVER_OPERATORS_ALIASES
  ),
  sqlServerPoolMax: parseInt(process.env.SQLSERVER_POOL_MAX),
  sqlServerPoolMin: parseInt(process.env.SQLSERVER_POOL_MIN),
  sqlServerPoolAcquire: parseInt(process.env.SQLSERVER_POOL_ACQUIRE),
  sqlServerPoolIdle: parseInt(process.env.SQLSERVER_POOL_IDLE),

  //Mongo database for business logic
  mongoLHost: process.env.MONGO_L_HOST,
  mongoLPort: parseInt(process.env.MONGO_L_PORT),
  mongoLDatabase: process.env.MONGO_L_DATABASE,
  mongoLUsername: process.env.MONGO_L_USERNAME,
  mongoLPassword: process.env.MONGO_L_PASSWORD,

  //Swagger documentation OpenApi 2.0
  basePath: process.env.BASE_PATH,
  swaggerPort: process.env.SWAGGER_PORT
    ? process.env.SWAGGER_PORT
    : process.env.PORT,
};
